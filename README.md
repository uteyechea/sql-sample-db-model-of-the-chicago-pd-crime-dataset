# SQL Sample DB model of the Chicago PD crime dataset

Provided SQL script to generate a database in SQL using the Chicago P.D. crime data set. 
This is a toy model, given the fact that many attributes found in the Chicago P.D. data set have been omitted. 
The selected attributes included in the SQL database are time, location, and crime type. 
This small subset of attributes have been proven to favor machine learning algorithms
tasked with predicting the when and where different types of crimes will be committed.
Thus, the selected attributes will serve to train my machine learning algorithms.

To use this script you need to download the Chicago PD data set, which you can find [here](https://data.cityofchicago.org/Public-Safety/Crimes-2019/w98m-zvie)
in the SQL script provided in this repository this file has been imported as a database and named chicago_crime_data.